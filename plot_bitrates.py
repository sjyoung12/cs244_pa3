from util.helper import *
import glob
import sys
from collections import defaultdict
import pickle
from matplotlib.font_manager import FontProperties
import os

def first(lst):
    return map(lambda e: e[0], lst)

def second(lst):
    return map(lambda e: e[1], lst)

compete_time = 0
add_competing_line = []

def parse_data(filename):
    global compete_time
    vid_band_points = []
    comp_band_points = []
    video_bitrates = []
    with open(filename,'rb') as pkl_file:
        info = pickle.load(pkl_file)
        chunks = info['chunks']
        for entry in chunks:
            bitrate = entry['bitrate']
            time = entry['time']
            video_bitrates.append((time,bitrate))
            #print "%s, %s" % (time,bitrate)
    compete_time = min(info['competing_bandwidths'].keys())
    vid_band_points = [(k,info['video_bandwidths'][k]) for k in sorted(info['video_bandwidths'], key=lambda key:key)]
    comp_band_points = [(k,info['competing_bandwidths'][k]) for k in sorted(info['competing_bandwidths'], key=lambda key:key)]
    data = {}
    data['video_bitrates'] = video_bitrates
    data['vid_band_points'] = vid_band_points
    data['comp_band_points'] = comp_band_points
    data['add_competing_line'] = add_competing_line
    data['bottleneck_rate'] = info['bottleneck_rate']
    return data

def plot_that_shiz(data, plot_filename, dir):
    fontP = FontProperties()
    fontP.set_size('x-small')
    video_bitrates = data['video_bitrates']
    vid_band_points = data['vid_band_points']
    comp_band_points = data['comp_band_points']
    add_competing_line = data['add_competing_line']

    fig = plt.figure()
    ax = fig.add_subplot(111)

    
    l1 = ax.plot(first(video_bitrates), second(video_bitrates), lw=2, label="Video bitrate")
    l2 = ax.plot(first(vid_band_points),second(vid_band_points),lw=2,label="Video throughput")
    l3 = ax.plot(first(comp_band_points),second(comp_band_points),lw=2,label="Competing throughput")

    for i in range(int(plt.ylim()[1])):
        add_competing_line.append((compete_time,i))
    l4 = ax.plot(first(add_competing_line),second(add_competing_line), lw=2, label="Competing flow start")
    
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Kb/s')

    ax.set_xlim(0,max(first(video_bitrates)))

    box = ax.get_position()
    ax.set_position([box.x0,box.y0,box.width*0.7,box.height])

    


    
    plt.title("Bottleneck bandwidth: %d Kb/s" % data['bottleneck_rate'])
    buffer_filename = os.path.join(dir, 'buffer.pkl')
    if not os.path.exists(buffer_filename):
        lns = l1+l2+l3+l4
    else:
        buffer_occupancies_list = []
        with open (buffer_filename,'r') as buffer_file:
            buffer_occupancies = pickle.load(buffer_file)
            buffer_occupancies_list = \
                [(k,buffer_occupancies[k]) for k in sorted(buffer_occupancies, key=lambda key:key)]
        ax2 = ax.twinx()
        l5 = ax2.plot(first(buffer_occupancies_list),second(buffer_occupancies_list),\
                    lw=2,color='y',label="Buffer occupancy")
        ax2.set_ylabel('Buffer Occupancy (s)')
        ax2.set_position([box.x0,box.y0,box.width*0.7,box.height])
        ax2.set_ylim(0,250)
        ax2.set_xlim(0,max(first(video_bitrates)))
        lns = l1+l2+l3+l4+l5

    labs = [l.get_label() for l in lns]
    plt.legend(lns,labs,loc='upper left',bbox_to_anchor=(1.13,0.5),prop=fontP)
    plt.savefig(plot_filename)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--out',
                        help="Save plot to output file, e.g.: --out plot.png",
                        dest="out",
                        default=None)

    parser.add_argument('-f', '--file',
                        dest="file",
                        help="pkl file to read",
                        required=True)

    args = parser.parse_args()
    data = parse_data(args.file)
    plot_that_shiz(data, args.out, os.path.dirname(args.file))

