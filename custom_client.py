from scapy.all import *
import sys
import os
from math import *
from httplib import *
import pickle
from numpy import mean
from common import *

if len(sys.argv) != 6:
    print "Usage: python custom_client.py <SERVER_IP> <DURATION> <CONSERVATISM> <DIR> <ALGORITHM>"
    exit()

if sys.argv[5] not in valid_algs:
    print "Algorithm " + sys.argv[5] + " not a valid algorithm"
    exit()

SERVER_IP = sys.argv[1]
DURATION = int(sys.argv[2])
CONSERVATISM = float(sys.argv[3])
DIR = sys.argv[4]
ALGORITHM = sys.argv[5]
assert CONSERVATISM >= 0 and CONSERVATISM <= 1
assert DURATION > 0 and DURATION < 10000
assert os.path.exists(DIR)
MOVING_AVG_LEN_VID = get_moving_average_len_video(ALGORITHM)
NONEMPTY_THRESHOLD = 40
FILENAMES = {235:'502473679.ismv',
            375:'504952236.ismv',
            560:'504780856.ismv',
            750:'504476069.ismv',
            1050:'502680635.ismv',
            1750:'504235388.ismv',
            2350:'506260537.ismv',
            3000:'507359414.ismv'}
CHUNKS_PER_REQUEST = 1
if ALGORITHM == 'bigchunk':
    CHUNKS_PER_REQUEST = 5

def estimate_bitrate_nonempty(prev_bandwidths):
    if len(prev_bandwidths) == 0:
        return 3000
    estimated = mean(prev_bandwidths)
    candidates = [i for i in FILENAMES.keys() if i <= estimated * (1 - CONSERVATISM)]
    highest_cand = max(candidates) if len(candidates) > 0 else 235
    lowest_higher_cand = 3000
    if (highest_cand < 3000):
        lowest_higher_cand = min([i for i in FILENAMES.keys() if i > highest_cand])
    if buffer_seconds > NONEMPTY_THRESHOLD:
        return lowest_higher_cand
    else:
        return highest_cand

def get_80th_percentile(prev_bandwidths):
    index = int(floor(0.8 * len(prev_bandwidths)))
    assert index < len(prev_bandwidths)
    return sorted(prev_bandwidths)[index]
     
#Takes prev_bandwidths, a list of the last ten measured bandwidths
def estimate_bitrate(prev_bandwidths):
    if ALGORITHM == 'nonempty':
        return estimate_bitrate_nonempty(prev_bandwidths)
        
    if len(prev_bandwidths) == 0:
        return 3000
        
    if ALGORITHM == '80p':
        estimated = get_80th_percentile(prev_bandwidths)
    else:
        estimated = mean(prev_bandwidths)
        
    candidates = [i for i in FILENAMES.keys() if i <= estimated * (1 - CONSERVATISM)]
    bitrate = max(candidates) if len(candidates) > 0 else 235
    print "Estimated throughput: %.2f, requesting %d Kb/s" % (estimated, bitrate)
    return bitrate

def get_next_chunk_range(bitrate):
    bytes_per_sec = bitrate * 1000 / 8
    start_index = buffer_seconds_total * bytes_per_sec
    end_index = start_index + (4 * bytes_per_sec * CHUNKS_PER_REQUEST) - 1
    return (start_index, end_index)
    
def buffer_has_space():
    if buffer_seconds <= 240:
        return True
    return False

def note_bandwidth(b):
    prev_bandwidths.append(b)
    if (len(prev_bandwidths) > MOVING_AVG_LEN_VID):
        prev_bandwidths.pop(0)

def request_chunk(chunk_range,bitrate,conn):
    print "%.2f: Sending request: (%d - %d) at %s Kbps, Buffer Seconds %s" % (time.time() - start_time, chunk_range[0], chunk_range[1], bitrate, buffer_seconds)
    global buffer_seconds
    global buffer_seconds_total
    buffer_seconds = buffer_seconds + 4*CHUNKS_PER_REQUEST
    buffer_seconds_total += 4*CHUNKS_PER_REQUEST
    
    bytes_requested = chunk_range[1] - chunk_range[0] + 1
    requested_file = '/%s/range/%d-%d' % (FILENAMES[bitrate], chunk_range[0], chunk_range[1])
    send_time = time.time()
    
    conn.request('GET', requested_file)
    response = conn.getresponse()
    response.read()
    receive_time = time.time()
    headers = response.getheaders()
    bytes_received = int(response.getheader('content-length'))
    
    time_to_receive = receive_time - send_time
    print "Time to receive %d bytes: %.3f seconds (%.2f Kb/s)" % (bytes_received,time_to_receive, (bytes_received * 8 / 1000.0) / time_to_receive)
    assert bytes_received == bytes_requested #Probably won't be right now...
    current_bandwidth = (float(bytes_requested) / float(time_to_receive)) * 8 / 1000
    note_bandwidth(current_bandwidth)

def start_competing_flow():
    print '\n\nSTARTING COMPETING FLOW\n\n'
    os.system('wget -q -O /dev/null %s/%s/%s/%s' % (SERVER_IP, '502473679.ismv', 'range', '0-'))

def round_down(num, divisor):
    return int(num) - (int(num) % divisor)

"""
Main program
"""
competing_flow_start = DURATION / 2
buffer_seconds = 0
buffer_seconds_total = 0
competing_flow_started = False
start_time = time.time()
playback_start_time = None
last_time = start_time
elapsed = 0.0
buffer_occupancy = {}

conn = HTTPConnection(SERVER_IP, strict=False)
prev_bandwidths = []

while elapsed <= DURATION:
    if not competing_flow_started and elapsed >= competing_flow_start:
        pid = os.fork()
        if pid == 0:
            # Child process
            start_competing_flow()
            exit()
        else:
            # Parent process
            competing_flow_started = True
    if (buffer_has_space()):
        bitrate_desired = estimate_bitrate(prev_bandwidths)
        next_chunk_range = get_next_chunk_range(bitrate_desired)
        request_chunk(next_chunk_range,bitrate_desired,conn)
        if playback_start_time is None:
            playback_start_time = time.time()
    else:
        print "sleeping because buffer full"
        time.sleep(4)
    elapsed = time.time() - playback_start_time
    buffer_seconds = buffer_seconds_total - round_down(elapsed, 4)
    buffer_occupancy[elapsed] = buffer_seconds

with open('%s/buffer.pkl' % DIR, 'wb') as f:
    pickle.dump(buffer_occupancy, f)

