from scapy.all import *
import sys
import os
from math import ceil
import pickle
from plot_bitrates import *
from numpy import mean
from common import *

"""
Parameters
"""
if len(sys.argv) != 5:
    print "Usage: python analyze_flow.py <DIR_NAME> <BOTTLENECK_RATE> <SERVER_IP> <ALGORITHM>"
    exit()

ALGORITHM = sys.argv[4]
if ALGORITHM not in valid_algs:
    print "%s is not a valid algorithm!" % ALGORITHM
    exit()

dirname = sys.argv[1]
filename = os.path.join(dirname, 'out.pcap')
SERVER_IP = sys.argv[3]
assert os.path.exists(filename)
# Initialize to -1, these will be learned dynamically from the trace
NETFLIX_PORT = -1
WGET_PORT = -1
# Granularity for throughput estimates, in seconds
BW_ESTIMATE_BUCKET_SECS = 4
# Throughput of the bottleneck link (Kb/s)
BOTTLENECK_RATE = int(sys.argv[2]) * 1000
IP_HDR_LEN = 20
TCP_HDR_LEN = 32
MOVING_AVG_LEN_COMP = 10
MOVING_AVG_LEN_VID = get_moving_average_len_video(ALGORITHM)

# Simple enum indicating the current status of the video flow
class FlowStatus:
    INACTIVE = 0
    REQUESTED = 1
    ACTIVE = 2

bitrates = {'679':235,
            '236':375,
            '856':560,
            '069':750,
            '635':1050,
            '388':1750,
            '537':2350,
            '414':3000}

"""
Helper functions
"""
 
def bytes_to_kbits(num_bytes):
    return (8 * num_bytes) / float(1000) # cuz it's a rate yo 
    
def get_bitrate_for_filename(filename):
    suffix = filename.split('.')[0][-3:]
    return bitrates[suffix] if suffix in bitrates else -1

def print_bw_estimates(bw_estimates):
    for k in sorted(bw_estimates, key=lambda key:key):
        print '%.5f\t%.2f' % (k, bw_estimates[k])

def print_requests(requests):
    print '\nHTTP Requests\ntime\tfilename\tbitrate\tlength\trange'
    for r in requests:
        print '%.2f\t%s\t%d\t%d\t%d-%d' % (r['time'], r['filename'], \
                r['bitrate'], r['end'] - r['start'] + 1, r['start'], r['end'])

def round_up_to_multiple_of(x, base):
    return int(base * round(float(x)/base)) + base

# Extrace byte request range from URL params to the GET request
def extract_range(url_params):
    result = re.search(r"^(\d*)-(\d*)", url_params)
    start = int(result.group(1)) if result.group(1) != '' else 0
    end = int(result.group(2)) if result.group(2) != '' else 0
    return start, end

# Extract the Content-Length field from an HTTP response    
def extract_content_length(pkt):
    if Raw not in pkt:
        return None
    result = re.search(r"Content-Length: (\d*)", pkt[Raw].load)
    if result is None:
        return None
    return int(result.group(1)) if result.group(1) != '' else None

"""
cur_video_flow functions
"""
def reset_cur_video_flow():
    global cur_video_flow 
    cur_video_flow = {'status':FlowStatus.INACTIVE, 'exp_bytes':-1, 'start_time':-1.0, 'bytes_so_far':-1, 'last_updated':-1, 'seqnos':set([]), 'first_seqno':0} 
    
def update_cur_video_flow_for_request(new_entry, bw_video, time):
    global cur_video_flow
    # Update status of cur_video_flow
    if cur_video_flow['status'] == FlowStatus.ACTIVE:
        # Got a new request while active, so current flow must be finished
        add_new_bw_video_estimate(bw_video)
    elif cur_video_flow['status'] == FlowStatus.REQUESTED:
        print "Got consecutive requests, resetting cur_video_flow"
        reset_cur_video_flow()
    cur_video_flow['status'] = FlowStatus.REQUESTED
    cur_video_flow['start_time'] = time
    cur_video_flow['last_updated'] = new_entry['time']
    cur_video_flow['exp_bytes'] = new_entry['end'] - new_entry['start'] + 1

def update_cur_video_flow_for_response(time, resp_length, seqno):
    global cur_video_flow
    if cur_video_flow['status'] != FlowStatus.REQUESTED:
        # Probably a response for a audio chunk
        return
    if resp_length != cur_video_flow['exp_bytes']:
        print "### Mismatched HTTP response, continuing"
    # Activate flow
    cur_video_flow['status'] = FlowStatus.ACTIVE
    #cur_video_flow['start_time'] = time
    cur_video_flow['last_updated'] = time
    cur_video_flow['bytes_so_far'] = 0
    cur_video_flow['first_seqno'] = seqno

def add_new_bw_video_estimate(bw_video):
    global cur_video_flow    
    global video_flow_tp_estimates  
    assert cur_video_flow['status'] == FlowStatus.ACTIVE
    
    if cur_video_flow['bytes_so_far'] < 0.8 * cur_video_flow['exp_bytes']:
        return
    
    dl_time = cur_video_flow['last_updated'] - cur_video_flow['start_time']
    new_est = bytes_to_kbits(cur_video_flow['exp_bytes']) / dl_time
    
    print "%.4f: %.2f kb/s (%d bytes in %.4f secs)" % (cur_video_flow['last_updated'], new_est, cur_video_flow['bytes_so_far'], dl_time)
    if abs(cur_video_flow['bytes_so_far'] - cur_video_flow['exp_bytes']) > 10000:
        print "#### Expected %d bytes, got %d" % \
              (cur_video_flow['exp_bytes'], cur_video_flow['bytes_so_far'])
    
    video_flow_tp_estimates.append(new_est)
    if len(video_flow_tp_estimates) > MOVING_AVG_LEN_VID:
        video_flow_tp_estimates.pop(0)
        
    bw_video[cur_video_flow['last_updated']] = mean(video_flow_tp_estimates)
    reset_cur_video_flow()

"""
Other functions
"""
def parse_http_request(pkt, time):
    url_tokens = str(pkt[Raw]).split('/')
    if len(url_tokens) < 4 or url_tokens[0] != 'GET ' \
            or not url_tokens[1].endswith('.ismv') or url_tokens[2] != 'range':
        return None
    new_entry = {}
    new_entry['filename'] = url_tokens[1]
    new_entry['bitrate'] = get_bitrate_for_filename(url_tokens[1])
    new_entry['time'] = time
    new_entry['start'], new_entry['end'] = extract_range(url_tokens[3])
    return new_entry
    
def handle_http_request(pkt, time, requests, bw_video):
    global NETFLIX_PORT
    global WGET_PORT
    
    new_entry = parse_http_request(pkt, time)
    if new_entry is None:
        return
        
    # Learn Netflix and wget ports
    if len(requests) == 0:
        # First request in trace, so TCP.sport must be the Netflix client port
        NETFLIX_PORT = pkt[TCP].sport
        print "\n\nVideo flow starting on client port %d\n\n" % NETFLIX_PORT
    elif pkt[TCP].sport != NETFLIX_PORT:
        # First request not to use NETFLIX_PORT, so must be wget port
        WGET_PORT = pkt[TCP].sport
        print "\n\nCompeting flow starting on client port %d\n\n" % WGET_PORT
        return # don't want to include this request
    
    requests.append(new_entry)
    update_cur_video_flow_for_request(new_entry, bw_video, time)
        
def handle_incoming_tcp_packet(pkt, time, bw_video, bw_competing):
    global competing_seqnos
    global competing_last_ack
    seqno = pkt[TCP].seq
    
    cl = extract_content_length(pkt)
    if cl is None:
        return
    if pkt[TCP].dport == NETFLIX_PORT:
        # HTTP response belonging to video flow
        update_cur_video_flow_for_response(time, cl, seqno)
        
    elif pkt[TCP].dport == WGET_PORT:
        # HTTP response belonging to competing flow
        competing_last_ack = seqno
    
def handle_outgoing_tcp_packet(pkt, time, bw_video, bw_competing):
    global competing_last_ack
    
    if (pkt[TCP].flags & 16) == 0: # Check ACK bit in TCP flags
        return
    
    ackno = pkt[TCP].ack
    
    if pkt[TCP].sport == NETFLIX_PORT and cur_video_flow['status'] == FlowStatus.ACTIVE:
        if ackno < cur_video_flow['first_seqno']:
            # This probably means we sent an ack for the chunk before the current one.
            print "#### Got ackno %d above first_seqno %d" % (ackno, cur_video_flow['first_seqno'])
            return
        cur_video_flow['bytes_so_far'] = ackno - cur_video_flow['first_seqno']
        cur_video_flow['last_updated'] = time
        
    elif pkt[TCP].sport == WGET_PORT:
        if competing_last_ack == -1:
            # Sent an ack from the WGET port before we started the competing flow...?
            return
        if ackno < competing_last_ack:
            # Resequence competing flow
            competing_last_ack = ackno
            return
        # Ack for competing flow
        bytes_acked = ackno - competing_last_ack
        bucket = round_up_to_multiple_of(time, BW_ESTIMATE_BUCKET_SECS)
        if bucket not in bw_competing:
            bw_competing[bucket] = 0.0
        bw_competing[bucket] += bytes_to_kbits(bytes_acked)
        competing_last_ack = ackno

def handle_packet(pkt, time, requests, bw_video, bw_competing): 
    if pkt[IP].dst == SERVER_IP \
            and TCP in pkt and Raw in pkt \
            and pkt[Raw].load.startswith('GET'):
        # HTTP request packet
        handle_http_request(pkt, time, requests, bw_video)
    elif pkt[IP].src == SERVER_IP and TCP in pkt:
        handle_incoming_tcp_packet(pkt, time, bw_video, bw_competing)
    elif pkt[IP].dst == SERVER_IP and TCP in pkt:
        handle_outgoing_tcp_packet(pkt, time, bw_video, bw_competing)


def write_chunk_maps(chunks,vid_b,comp_b,txt_filename,pkl_filename):
    with open(txt_filename,'w') as text_out:
        text_out.write("start        \tend        \tfilename        \tbitrate\n")
        for k in sorted(chunks, key=lambda d: d['start']):
            text_out.write('%d   \t%d   \t%s    \t%d\n' % \
                    (k['start'], k['end'], k['filename'], k['bitrate']))
    with open(pkl_filename,'wb') as pickle_out:
        obj = {}
        obj['chunks'] = chunks
        obj['video_bandwidths'] = vid_b
        obj['competing_bandwidths'] = comp_b
        obj['bottleneck_rate'] = BOTTLENECK_RATE
        pickle.dump(obj,pickle_out)

def compute_moving_average(bucketed):
    estimates = {}
    cur = []
    for bucket in sorted(bucketed, key=lambda key:key):
        est = float(bucketed[bucket]) / BW_ESTIMATE_BUCKET_SECS
        cur.append(est)
        if len(cur) > MOVING_AVG_LEN_COMP:
            cur.pop(0)
        estimates[bucket] = mean(cur)
    return estimates

def parse_trace(filename):
    # Maps time to video flow throughput estimate at that time
    bw_video = {}
    # Maps a bucket (in seconds) to the competing flow throughput estimate for
    # that bucket.  E.g. when BW_ESTIMATE_BUCKET_SECS = 5, 
    # bw_competing[10] is the average throughput over the interval (5, 10]
    bw_competing = {}
    requests = []
    
    reset_cur_video_flow()
    pr = PcapReader(filename)
    counter = 0
    for p in pr:
        pkt = p.payload
        counter += 1
        if counter == 1:
            start_time = float(p.time)
        
        time = float(p.time) - start_time
        if IP in pkt:
            handle_packet(pkt, time, requests, bw_video, bw_competing)

    return requests, \
        {k:mean(estimates) for k, estimates in bw_video.iteritems()}, \
        compute_moving_average(bw_competing)
"""
Main program logic
"""
competing_seqnos = set([])
competing_last_ack = -1
video_flow_tp_estimates = []
cur_video_flow = {}
requests, bw_video, bw_competing = parse_trace(filename)
print "Found %d HTTP requests" % len(requests)
print "\nVideo flow throughput estimates"
print_bw_estimates(bw_video)
print "\nCompeting flow throughput estimates"
print_bw_estimates(bw_competing)

# Discard all estimates that seem spuriously high
to_delete = []
for time, est in bw_video.iteritems():
    if est > 2 * BOTTLENECK_RATE:
        to_delete.append(time)
for time in to_delete:
    del bw_video[time]
    
base_name = 'out'
txt_filename = os.path.join(dirname, base_name + '.txt')
pkl_filename = os.path.join(dirname, base_name + '.pkl')
plot_filename = os.path.join(dirname, base_name + '.png')
write_chunk_maps(requests, bw_video, bw_competing,
                 txt_filename, pkl_filename)

# Create the plot
data = parse_data(pkl_filename)
plot_that_shiz(data, plot_filename, dirname)
