sudo mkdir pcap3Mbs
sudo mkdir pcap5Mbs
cd pcap3Mbs
wget www.stanford.edu/~atrytko/3Mbsout.pcap
mv 3Mbsout.pcap out.pcap
cd ../pcap5Mbs
wget www.stanford.edu/~atrytko/5Mbsout.pcap
mv 5Mbsout.pcap out.pcap
cd ..
