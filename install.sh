if [ ! `id -u` -eq 0 ]
then
    echo "Must be run with sudo!"
    exit
fi

# Install ipfw (dummynet)
tar -zxvf ipfw3.tgz
cd ipfw3-2012
make
sudo insmod ./kipfw-mod/ipfw_mod.ko
sudo cp ipfw/ipfw /usr/local/sbin
cd ..

# Install scapy
tar -zxvf scapy-latest.tar.gz
cd scapy-2.1.0
sudo python setup.py install
cd ..

