valid_algs = ['none', 'nonempty', '80p', 'bigchunk']

def get_moving_average_len_video(alg):
    if alg == 'bigchunk':
        return 2
    else:
        return 10
