if [ ! `id -u` -eq 0 ]
then
    echo "Must be run with sudo!"
    exit
fi

EXPECTED_ARGS=5

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: sudo ./run.sh <DURATION> <BANDWIDTH> <CONSERVATISM> <DIR> <ALG>"
  exit
fi
    
duration=$1
bw=$2 # in Mb/s
conservatism=$3
dir=$4
alg=$5
echo "Writing to directory "$dir
if [ -f $dir/out.png ];
then
    # Plot already exists, terminate
    echo "Plot exists!"
    exit
fi

ip=$(curl ifconfig.me)

sudo rm -rf $dir
mkdir $dir

echo "Server IP address: "$ip
echo "Bottleneck rate: "$bw" Mb/s"

# Clean up
ps ax|grep "python server.py"|awk '{print $1}'|sudo xargs kill
sudo killall -9 tcpdump 

# Start server
sudo python server.py &

# Initialize dummynet
sudo insmod ipfw3-2012/kipfw-mod/ipfw_mod.ko
sudo cp ipfw3-2012/ipfw/ipfw /usr/local/sbin

# Set bottleneck bandwidth
sudo ipfw -q flush
sudo ipfw add pipe 1 in proto tcp
sudo ipfw pipe 1 config bw $bw'Mbit/s'
sudo ipfw pipe list

# Capture trace for custom client
sudo tcpdump -i eth0 -w $dir/out.pcap &
python custom_client.py $ip $duration $conservatism $dir $alg
sudo killall -9 tcpdump 
ps ax|grep "python server.py"|awk '{print $1}'|sudo xargs kill

# Clear bottleneck bandwidth
sudo ipfw -f flush
sudo ipfw -f pipe flush 

# Generate plot for trace
python analyze_flow.py $dir $bw $ip $alg
if [ -f $dir/out.png ];
then
    # Plot created successfully, remove pcap file
    sudo rm $dir/out.pcap
fi
