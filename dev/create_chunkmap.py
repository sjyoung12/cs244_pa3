import os 
import pickle
import sys

"""
Iterates over a directory full of pkl files (outputted by analyze_flow.py), one
per run at a hardcoded bitrate.  

Outputs a map {bitrate : ranges}, where ranges is an ordered list of (start, 
end) tuples corresponding to byte range requests.
"""

if len(sys.argv) != 2:
    print "usage: python create_chunkmap.py <root dir containing pkl files>"
    exit()

pkl_dir = sys.argv[1]
assert os.path.exists(pkl_dir)
files = os.listdir(pkl_dir)

chunkmap = {}
chunks_by_bitrate = {}

for pkl_file in files:
    with open(os.path.join(pkl_dir, pkl_file), 'rb') as f:
        data = pickle.load(f)
    bitrates = set([])
    for chunk in data['chunks']:
        bitrate = chunk['bitrate']
        bitrates.add(bitrate)
        if bitrate not in chunks_by_bitrate:
            chunks_by_bitrate[bitrate] = []
        chunks_by_bitrate[bitrate].append(chunk)
    print "%s: %d bitrates" % (pkl_file, len(bitrates))
for bitrate, chunks in chunks_by_bitrate.iteritems():
    chunkmap[bitrate] = []
    for k in sorted(chunks, key=lambda d: d['start']):
        #print '%d: %d - %d' % (k['bitrate'], k['start'], k['end'])
        if len(chunkmap[bitrate]) == 0:
            chunkmap[bitrate].append((k['start'], k['end']))
        else:
            if k['start'] == chunkmap[bitrate][-1][1] + 1:
                chunkmap[bitrate].append((k['start'], k['end']))
    print '%d: %d ranges' % (bitrate, len(chunkmap[bitrate]))

for bitrate, ranges in chunkmap.iteritems():
    for i in range(len(ranges) - 1):
        assert ranges[i][1] == ranges[i+1][0] - 1

with open('chunkmap.pkl', 'wb') as f:
    pickle.dump(chunkmap, f)
