ip=$(curl ifconfig.me)

for l in 2 3 5; do
    for pf in 0.5 0.6 0.7 0.8 0.9 1.0; do
        echo "Running with bw limit "$l", pf "$pf
        dir = 'run_l'$l'_p'$pf
        sudo ./run.sh $ip 500 $l $pf
        if [ -f $dir/out.png ];
        then
            rm $dir/out.pcap
        fi
    done
done
