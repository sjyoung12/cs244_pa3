import os
import sys
import pickle


tokens = {'502473679.ismv':'c=us&n=32&v=3&e=1362557838&t=PkMoetzXr0NaJ5ANnKOE_-TYRuE&d=silverlight&p=5.6uALf6AzQlMKJcom7TyThQZK5PYEhHWPaVBMQ8ikQ5c&random=1962206779',
'504952236.ismv':'c=us&n=32&v=3&e=1362557838&t=h1Zl34mN6Iik0Dq93SAimEysPAE&d=silverlight&p=5.6uALf6AzQlMKJcom7TyThQZK5PYEhHWPaVBMQ8ikQ5c&random=1492879685',
'504780856.ismv':'c=us&n=32&v=3&e=1362557838&t=cysrjaePDD_viTH3EZfb9dGfkq8&d=silverlight&p=5.6uALf6AzQlMKJcom7TyThQZK5PYEhHWPaVBMQ8ikQ5c&random=1535158020',
'504476069.ismv':'c=us&n=32&v=3&e=1362557838&t=KjZX3a9Y5DJFwGLdIdCEyHsrELc&d=silverlight&p=5.6uALf6AzQlMKJcom7TyThQZK5PYEhHWPaVBMQ8ikQ5c&random=793735239',
'502680635.ismv':'c=us&n=32&v=3&e=1362557838&t=PJRpbVWDvWSDzdII26QRWGwDDeg&d=silverlight&p=5.6uALf6AzQlMKJcom7TyThQZK5PYEhHWPaVBMQ8ikQ5c&random=932161452',
'504235388.ismv':'c=us&n=32&v=3&e=1362557838&t=G7aE-dgpwFkfCmL1KfCzSjnz_z0&d=silverlight&p=5.6uALf6AzQlMKJcom7TyThQZK5PYEhHWPaVBMQ8ikQ5c&random=1572936080',
'506260537.ismv':'c=us&n=32&v=3&e=1362557838&t=fw_6S0xFwmvtGA8HewduCkEPHW8&d=silverlight&p=5.6uALf6AzQlMKJcom7TyThQZK5PYEhHWPaVBMQ8ikQ5c&random=41829078',
'507359414.ismv':'c=us&n=32&v=3&e=1362557838&t=rgjfhZBKfMt44ApOQSC8oDEPPrU&d=silverlight&p=5.6uALf6AzQlMKJcom7TyThQZK5PYEhHWPaVBMQ8ikQ5c&random=1602881792'}

if len(sys.argv) != 2:
    print "usage: python download_all_chunks.py <dir containing pkl files>"

root_dir = os.getcwd()
print "Root dir is %s" % root_dir
pkl_dir = sys.argv[1]
assert os.path.exists(pkl_dir)
files = os.listdir(pkl_dir)
print files

SERVER_IP = '198.189.255.2'

template = "http://%s/%s/range/%d-%d?%s"
for pkl_file in files:
    with open(os.path.join(pkl_dir, pkl_file), 'rb') as f:
        data = pickle.load(f)
        
    chunks_dir = os.path.join(pkl_dir, pkl_file.split('.')[0].split('_')[2])
    if not os.path.exists(chunks_dir):
        os.mkdir(chunks_dir)
    os.chdir(chunks_dir)
    print "\n\nNow in %s" % chunks_dir
     
    
    for k in sorted(data['chunks'], key=lambda d: d['start']):
        url = template % (SERVER_IP, k['filename'], k['start'], k['end'], tokens[k['filename']])
        wget = "wget '%s'" % url
        #print wget
        os.system(wget)
                
    os.chdir(root_dir)
    
