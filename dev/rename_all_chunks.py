import os
import sys


if len(sys.argv) != 2:
    print "usage: python rename_all_chunks.py <root dir containing chunk files>"
    
chunk_dir = sys.argv[1]
assert os.path.exists(chunk_dir)

subdirs = os.listdir(chunk_dir)
for subdir in subdirs:
    full_path = os.path.join(chunk_dir, subdir)
    files = os.listdir(full_path)
    for file in files:
        old_path = os.path.join(full_path, file)
        tokens = file.split('?')
        new_path = os.path.join(full_path, tokens[0])
        os.rename(old_path, new_path)
