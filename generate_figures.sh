if [ ! `id -u` -eq 0 ]
then
    echo "Must be run with sudo!"
    exit
fi

duration=500
default_consv=0.4
bw=7

# Fig. 4/Fig. 5 (top left) - default parameters
sudo ./run.sh $duration $bw $default_consv out_${bw}Mbs_${default_consv}c none

# Fig. 5 (top right) - 0.2 conservatism
sudo ./run.sh $duration $bw 0.2 out_${bw}Mbs_0.2c none

# Fig. 5 (bottom) - 0.0 conservatism
sudo ./run.sh $duration $bw 0.0 out_${bw}Mbs_0.0c none

# Fig. 6 (top) - 0.4 conservatism and 80p filtering
sudo ./run.sh $duration $bw $default_consv out_${bw}Mbs_${default_consv}c_80p 80p

# Fig. 6 (bottom) - 0.2 conservatism and 80p filtering
sudo ./run.sh $duration $bw 0.2 out_${bw}Mbs_0.2c_80p 80p

# Fig. 7 - 5x larger chunk sizes
sudo ./run.sh $duration $bw $default_consv out_${bw}Mbs_${default_consv}c_bigchunk bigchunk

# Fig. 8 - nonempty algorithm extension
sudo ./run.sh $duration $bw $default_consv out_${bw}Mbs_${default_consv}c_nonempty nonempty

# With 0.1 conservatism
# sudo ./run.sh $duration $bw 0.1 out_${bw}Mbs_0.1c none

# With 0.3 conservatism
# sudo ./run.sh $duration $bw 0.3 out_${bw}Mbs_0.3c none

# With nonempty optimization in custom client and no conservatism
# sudo ./run.sh $duration $bw 0.0 out_${bw}Mbs_0.0c_nonempty nonempty

# Move all plots into $plots_dir
plots_dir="plots"
if [ ! -d "$plots_dir" ]; then
    mkdir $plots_dir
fi

for dir in `ls -1d out_*`; do
    if [ -f $dir/out.png ];
    then
        sudo cp $dir/out.png $plots_dir/$dir.png
    fi
done
