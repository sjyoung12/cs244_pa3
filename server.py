#!/usr/bin/python
from SocketServer import ThreadingMixIn
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from os import curdir, sep
import os
import re

#Mostly copied from:
#http://www.acmesystems.it/python_httpserver

PORT_NUMBER = 80

def extract_range(url_params):
    try:
        derp = url_params.split('/')
        result = re.search(r"(\d*)-(\d*)", derp[2])
        start = int(result.group(1)) if result.group(1) != '' else 0
        end = int(result.group(2)) if result.group(2) != '' else 1024*1024*1024
        return end - start + 1
    except:
        return 0

class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            self.protocol_version = 'HTTP/1.1'
            filepath = self.path[1:]
            bytes_requested = extract_range(filepath)

            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.send_header('Content-Length', bytes_requested)
            self.send_header('Connection', 'Keep-Alive')  
            self.end_headers()

            kb = '0' * 1024
            mb = '0' * 1024 * 1024
            four_mb = '0' * 1024 * 1024 * 4
            bytes_written = 0
            while bytes_written < bytes_requested:
                remaining = bytes_requested - bytes_written
                if remaining >= 1024 * 1024 * 4:
                    self.wfile.write(four_mb)
                    bytes_written += 1024 * 1024 * 4
                elif remaining >= 1024 * 1024:
                    self.wfile.write(mb)
                    bytes_written += 1024 * 1024
                elif remaining >= 1024:
                    self.wfile.write(kb)
                    bytes_written += 1024
                else:
                    self.wfile.write('0' * remaining)
                    bytes_written += remaining
        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)
            
class MultiThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    pass
    
try:
    #Create a web server and define the handler to manage the
    #incoming request
    server = MultiThreadedHTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER
    
    #Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
