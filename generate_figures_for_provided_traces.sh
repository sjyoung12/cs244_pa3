# Generate plots for the (provided) traces captured against the current Netflix client
sudo python analyze_flow.py pcap3Mbs 3 198.189.255.2 none
sudo python analyze_flow.py pcap5Mbs 5 198.189.255.2 none

# Move all plots into $plots_dir
plots_dir="plots"
if [ ! -d "$plots_dir" ]; then
    mkdir $plots_dir
fi

sudo cp pcap3Mbs/out.png $plots_dir/pcap3Mbs.png
sudo cp pcap5Mbs/out.png $plots_dir/pcap5Mbs.png
